package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserInfomation;
import service.StopRevivalService;



@WebServlet(urlPatterns = {"/stoprevival"})
public class StopRevivalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		UserInfomation userInfomation = new UserInfomation();
		userInfomation.setId(Integer.parseInt(request.getParameter("id")));
		userInfomation.setIs_deleted(Integer.parseInt(request.getParameter("is_deleted")));

		if (userInfomation.getIs_deleted() == 0) {

			new StopRevivalService().stopUpdate(userInfomation);

		} else if (userInfomation.getIs_deleted() == 1) {

			new StopRevivalService().revivalUpdate(userInfomation);

		}

		response.sendRedirect("management");




	}

}

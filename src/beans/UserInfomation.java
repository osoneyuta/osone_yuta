package beans;

import java.io.Serializable;

public class UserInfomation implements Serializable{

	private int id;
	private String login_id;
	private String name;
	private int branch_id;
	private int position_id;
	private String branch_name;
	private String position_name;
	private int is_deleted;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin_id() {
		return this.login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBranch_name() {
		return this.branch_name;
	}

	public void setBranch_name(String branch_name) {
			this.branch_name = branch_name;
	}

	public String getPosition_name() {
		return this.position_name;
	}

	public void setPosition_name(String position_name) {

		this.position_name = position_name;
	}

	public int getIs_deleted() {
		return this.is_deleted;
	}

	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}

	public int getBranch_id() {
		return this.branch_id;
	}

	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public int getPosition_id() {
		return this.position_id;
	}

	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}



}

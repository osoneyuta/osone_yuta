package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserInfomation;
import exception.SQLRuntimeException;

public class UserInfomationDao {

	public List<UserInfomation> getInfomations(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.login_id as login_id, ");
			sql.append("users.name as name, ");
			sql.append("branches.name as branch_name, ");
			sql.append("positions.name as position_name, ");
			sql.append("users.is_deleted as is_deleted ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");



			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserInfomation> ret = toUserInfomationList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}



	private List<UserInfomation> toUserInfomationList(ResultSet rs)
			throws SQLException {

		List<UserInfomation> ret = new ArrayList<UserInfomation>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");
				int is_deleted = rs.getInt("is_deleted");

				UserInfomation infomation = new UserInfomation();
				infomation.setId(id);
				infomation.setLogin_id(login_id);
				infomation.setName(name);
				infomation.setBranch_name(branch_name);
				infomation.setPosition_name(position_name);
				infomation.setIs_deleted(is_deleted);

				ret.add(infomation);
			}


			return ret;

		} finally {
			close(rs);
		}






	}
}
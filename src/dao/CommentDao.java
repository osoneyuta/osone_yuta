package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert (Connection connection, Comment comment) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append("contribution_id");
			sql.append(",text");
			sql.append(",created_at");
			sql.append(",user_id");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(",?");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(",?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getContribution_id());
			ps.setString(2, comment.getText());
			ps.setInt(3, comment.getUser_id());
			ps.executeUpdate();

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

	}

	public List<Comment> getComments(Connection connection) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("comments.id as id, ");
			sql.append("comments.contribution_id as contribution_id, ");
			sql.append("comments.text as text, ");
			sql.append("comments.created_at as created_at, ");
			sql.append("users.name as name, ");
			sql.append("comments.user_id as user_id ");
			sql.append(" FROM ");
			sql.append("comments ");
			sql.append("INNER JOIN ");
			sql.append("contributions ");
			sql.append("ON ");
			sql.append("comments.contribution_id = contributions.id ");
			sql.append("INNER JOIN ");
			sql.append("users ");
			sql.append("ON ");
			sql.append("comments.user_id = users.id ");
			sql.append("ORDER BY comments.id DESC ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);

			return ret;

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<Comment> toCommentList(ResultSet rs) throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				int contribution_id = rs.getInt("contribution_id");
				String text = rs.getString("text");
				Timestamp created_at = rs.getTimestamp("created_at");
				String name = rs.getString("name");
				int user_id = rs.getInt("user_id");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setContribution_id(contribution_id);
				comment.setText(text);
				comment.setCreated_at(created_at);
				comment.setName(name);
				comment.setUser_id(user_id);

				ret.add(comment);

			}

			return ret;

		} finally {
			close(rs);
		}
	}

	public void delete (Connection connection, Comment comment) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE ");
			sql.append("FROM comments ");
			sql.append("WHERE ");
			sql.append("id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}

package beans;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private int contribution_id;
	private int user_id;
	private String name;
	private String text;
	private Timestamp created_at;

	public int getContribution_id() {
		return this.contribution_id;
	}
	public void setContribution_id(int contribution_id) {
		this.contribution_id = contribution_id;
	}

	public int getUser_id() {
		return this.user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public Timestamp getCreated_at() {
		return this.created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.SettingDao;
import utils.CipherUtil;


public class SettingService {

	public void edit(User user) {

		Connection connection = null;

		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            SettingDao settingDao = new SettingDao();
            settingDao.update(connection, user);

            commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void selfEdit(User user) {

		Connection connection = null;

		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            SettingDao settingDao = new SettingDao();
            settingDao.selfUpdate(connection, user);

            commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void passwordNotEdit(User user) {

		Connection connection = null;

		try {
			connection = getConnection();

            SettingDao settingDao = new SettingDao();
            settingDao.update(connection, user);

            commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void passwordNotSelfEdit(User user) {

		Connection connection = null;

		try {
			connection = getConnection();

            SettingDao settingDao = new SettingDao();
            settingDao.selfUpdate(connection, user);

            commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}

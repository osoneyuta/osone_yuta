package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginFilter
 */
@WebFilter("/*")
public class LoginFilter implements Filter {



	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		HttpSession session = req.getSession();

		if((req.getServletPath().equals("/commentdelete")  ||
				req.getServletPath().equals("/comment")  ||
				req.getServletPath().equals("/contribution") ||
				req.getServletPath().equals("/delete") ||
				req.getServletPath().equals("/login") ||
				req.getServletPath().equals("/logout")  ||
				req.getServletPath().equals("/setting") ||
				req.getServletPath().equals("/signup") ||
				req.getServletPath().equals("/stoprevival") ||
				req.getServletPath().equals("/index.jsp") ||
				req.getServletPath().equals("/management")) != true) {

			messages.add("そのURLは存在しません");
			session.setAttribute("messages", messages);
			resp.sendRedirect("./");
			return;
		}

		if (req.getServletPath().equals("/login") && session.getAttribute("loginUser") != null) {

			messages.add("既にログイン中です");
			session.setAttribute("messages", messages);
			resp.sendRedirect("./");
			return;

		}


		if (req.getServletPath().equals("/login") && session.getAttribute("loginUser") == null) {

			chain.doFilter(req, resp);
			return;
		}




		if (session.getAttribute("loginUser") == null) {


			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			resp.sendRedirect("login");
			return;
		}

		if ((req.getServletPath().equals("/management") == true ||
				req.getServletPath().equals("/setting") == true   ||
				req.getServletPath().equals("/signup") == true) == true &&
				session.getAttribute("position_id").equals(1) != true) {

			messages.add("権限がありません");
			session.setAttribute("messages", messages);
			resp.sendRedirect("./");
			return;
		}



		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {
	}





}

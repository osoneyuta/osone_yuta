<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>編集画面</title>

<script type="text/javascript">


function check(){

	if(window.confirm('編集してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

</script>

</head>
<body>


	<div>
	<c:if test="${ not empty messages }">
			<div class="messages">
				<ul>
					<c:forEach items="${messages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
	</c:if>
	</div>




	<form action="setting" method="post" onSubmit="return check()">
	<br/>




	<input type="hidden" name="id" value="${id}">


	<c:forEach items="${name}" var="name">

	ログインID:
	<input type="text" name="login_id" value="${name.login_id}"/>
	<br/>


	名称:
	<c:if test="${ empty failName}">
	<input type="text" name="name" value="${name.name}"/>
	<br/>
	</c:if>
	<c:if test="${ not empty failName }">
	<input type="text" name="name" value="${failName}" />
	</c:if>

	パスワード:
	<input name="password" id="password" value="${password}"/>
	<br/>

	パスワード確認:
	<input name="password_check" id="password_check" value="${passwordCheck}"/>
	<br/>


	<c:if test="${loginUser != id }">
	支店:
	<select name="branch_id">
	<c:forEach items="${branches}" var="branches">
	<c:choose>
		<c:when test="${name.branch_id == branches.id }">
		<option value="${branches.id}" selected>${branches.branch_name}</option>
		</c:when>
		<c:when test="${name.branch_id != branches.id }">
		<option value="${branches.id}" >${branches.branch_name}</option>
		</c:when>
	</c:choose>
	</c:forEach>
	</select>
	<br/>
	</c:if>

	<c:if test="${loginUser != id }">
	役職:
	<select name="position_id">
	<c:forEach items="${positions}" var="positions">
	<c:choose>
		<c:when test="${name.position_id == positions.id }">
		<option value="${positions.id}" selected>${positions.position_name}</option>
		</c:when>
		<c:when test="${name.position_id != positions.id }">
		<option value="${positions.id}" >${positions.position_name}</option>
		</c:when>
	</c:choose>
	</c:forEach>
	</select>
	<br/>
	</c:if>

	<input type="submit" value="編集" />
	<br/>

	</c:forEach>
	</form>

	<a href="management">戻る</a>
</body>
</html>
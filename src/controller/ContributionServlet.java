package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import service.ContributionService;

/**
 * Servlet implementation class ContributionServlet
 */
@WebServlet(urlPatterns = {"/contribution"})
public class ContributionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		request.setAttribute("id", session.getAttribute("loginUser"));

		request.getRequestDispatcher("/contribution.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		Contribution contribution = new Contribution();

		List<String> messages = new ArrayList<String>();


		// 改行コードを取り除いた文字列を配列に格納。
		String[] noNewLineText = request.getParameter("text").split("\r\n");

		// textの文字数を確認したい為、0で初期化。
		int textCount = 0;

		// textCountに文字数を足していく。
		for (int i = 0; i < noNewLineText.length; i++) {
			textCount += noNewLineText[i].length();
		}


		if (StringUtils.isBlank(request.getParameter("subject")) == true) {

			messages.add("件名が空です");
		} else if (request.getParameter("subject").length() >= 31) {

			messages.add("件名の文字数が30文字以内ではありません");
		} else {
			contribution.setSubject(request.getParameter("subject"));
		}

		if (StringUtils.isBlank(request.getParameter("category")) == true) {

			messages.add("カテゴリーが空です");
		} else if (request.getParameter("category").length() >= 11) {

			messages.add("カテゴリーの文字数が10文字以内ではありません");
		} else {
			contribution.setCategory(request.getParameter("category"));
		}


		if (StringUtils.isBlank(request.getParameter("text")) == true) {
			messages.add("本文が空です");
		} else if (textCount >= 1001) {

			messages.add("本文の文字数が1000文字以内ではありません");
		} else {
			contribution.setText(request.getParameter("text"));
		}

		// ログインしているユーザーIDをセッションから受け取りintにキャストしてidに格納。
		int id = (int) session.getAttribute("loginUser");

		contribution.setUserId(id);

		if (messages.size() == 0) {

			new ContributionService().Store(contribution);
			response.sendRedirect("./");

		} else {

			request.setAttribute("subject", request.getParameter("subject"));
			request.setAttribute("category", request.getParameter("category"));
			request.setAttribute("text", request.getParameter("text"));
			request.setAttribute("messages", messages);
			request.getRequestDispatcher("/contribution.jsp").forward(request, response);
		}


	}

}

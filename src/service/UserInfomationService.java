package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserInfomation;
import dao.UserInfomationDao;

public class UserInfomationService {

	public List<UserInfomation> getInfomations() {

		Connection connection = null;

		try {
			connection = getConnection();

			UserInfomationDao infomationDao = new UserInfomationDao();
			List<UserInfomation> ret = infomationDao.getInfomations(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

}

package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserInfomation;
import dao.SetNameDao;

public class SetNameService {

	public List<UserInfomation> getPositionNames() {

		Connection connection = null;

		try {
			connection = getConnection();

			SetNameDao setNameDao = new SetNameDao();
			List<UserInfomation> ret = setNameDao.getPositionNames(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserInfomation> getBranchNames() {

		Connection connection = null;

		try {
			connection = getConnection();

			SetNameDao setNameDao = new SetNameDao();
			List<UserInfomation> ret = setNameDao.getBranchNames(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	public List<UserInfomation> getUserName(int id) {

		Connection connection = null;

		try {
			connection = getConnection();

			SetNameDao setNameDao = new SetNameDao();
			List<UserInfomation> ret = setNameDao.getUserName(connection, id);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}


}

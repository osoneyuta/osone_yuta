package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.UserInfomation;
import dao.StopRevivalDao;

public class StopRevivalService {

	public void stopUpdate (UserInfomation userInfomation) {

		Connection connection = null;

		try {
			connection = getConnection();

			StopRevivalDao stopRevivalDao = new StopRevivalDao();
			stopRevivalDao.stop(connection, userInfomation);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public void revivalUpdate (UserInfomation userInfomation) {

		Connection connection = null;

		try {
			connection = getConnection();

			StopRevivalDao stopRevivalDao = new StopRevivalDao();
			stopRevivalDao.revival(connection, userInfomation);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}

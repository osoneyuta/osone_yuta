<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>

<script type="text/javascript">


function revivalCheck(){

	if(window.confirm('ユーザーを復活してもよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

function stopCheck(){

	if(window.confirm('ユーザーを停止してもよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

</script>
</head>
<body>


	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session"/>
	</c:if>



	<a href="signup">新規登録画面</a><br/>
	<table border="1">
		<tr>
			<th>ID</th>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>支店名</th>
			<th>役職名</th>
			<th>アカウント状況</th>
			<th>編集</th>
		</tr>

	<c:forEach items="${infomations}" var="infomations">
		<tr>
			<td>${infomations.id}</td>
			<td>${infomations.login_id}</td>
			<td>${infomations.name}</td>
			<td>${infomations.branch_name}</td>
			<td>${infomations.position_name}</td>
			<td>
			<c:if test="${infomations.id != loginUser }" >
			<c:choose>
				<c:when test="${infomations.is_deleted == 0 }">
					<form action="stoprevival" method="post" onSubmit="return stopCheck()" >
						<input type="hidden" name="id" value="${infomations.id }" />
						<input type="hidden" name="is_deleted" value="${infomations.is_deleted}" />
						<input type="submit" value="停止" />
					</form>
				</c:when>
				<c:when test="${infomations.is_deleted == 1 }">
					<form action="stoprevival" method="post" onSubmit="return revivalCheck()" >
						<input type="hidden" name="id" value="${infomations.id }" />
						<input type="hidden" name="is_deleted" value="${infomations.is_deleted }" />
						<input type="submit" value="復活" />
					</form>
				</c:when>
			</c:choose>
			</c:if>
			</td>
			<td><a href="setting?id=${infomations.id}">編集</a></td>
		</tr>




	</c:forEach>
	</table>

	<a href="./">戻る</a>

</body>
</html>
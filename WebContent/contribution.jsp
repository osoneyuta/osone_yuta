<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>

<script type="text/javascript">


function check(){

	if(window.confirm('投稿してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

</script>

</head>
<body>

	<c:if test="${ not empty messages }">
		<div class="messages">
			<ul>
				<c:forEach items="${messages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="messages" scope="session"/>
	</c:if>

	<div>
		<form action="contribution" method="post" onSubmit="return check()">
			<label for="subject">件名(30文字まで)</label><br>
			<input type="text" name="subject" id="subject" value="${subject}"maxlength="30"><br/>

			<label for="category">カテゴリー(10文字まで)</label><br>
			<input type="text" name="category" id="category" value="${category}"maxlength="10"><br/>

			<label for="text">本文(1000文字まで)</label><br>
			<textarea name="text" cols="100" rows="10"  id="text" >${text}</textarea>

			<input type="hidden"  name="id" value="${id}"><br/>

			<input type="submit" value="投稿">
		</form>

	</div><br/>
	<a href="./">戻る</a>
</body>
</html>
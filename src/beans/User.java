package beans;

import java.io.Serializable;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String login_id;
	private String password;
	private String name;
	private int branch_id;
	private int position_id;
	private int is_deleted;


	public void setId(int id) {
		this.id = id;
	}

	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBranchId(int branch_id) {
		this.branch_id = branch_id;
	}

	public void setPositionId(int position_id) {
		this.position_id = position_id;
	}

	public void setIsDeleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}

	public int getId() {
		return this.id;
	}

	public String getLoginId() {
		return this.login_id;
	}

	public String getPassword() {
		return this.password;
	}

	public String getName() {
		return this.name;
	}

	public int getBranchId() {
		return this.branch_id;
	}

	public int getPositionId() {
		return this.position_id;
	}

	public int getIsDeleted() {
		return this.is_deleted;
	}


}

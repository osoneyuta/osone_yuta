<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録画面</title>

<script type="text/javascript">


function check(){

	if(window.confirm('登録してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

</script>
</head>

<body>

	<div class="main-contents">
			<c:if test="${ not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
	</div>

	<form action="signup" method="post" onSubmit="return check()">
	<br/>

	<label for="login_id">ログインID</label>
	<input name="login_id" id="login_id" />
	<br/>

	<label for="password">パスワード</label>
	<input name="password" id="password" />
	<br/>

	<label for="password_check">パスワード（確認）</label>
	<input name="password_check" id="password_check" />
	<br/>

	<label for="name">名称</label>
	<input name="name" id="name" />
	<br/>

	<label for="branch_id">支店</label>
		<select name="branch_id">
			<option ></option>
			<c:forEach items="${branches}" var="branches">
				<option value="${branches.id}" >${branches.branch_name}</option>
			</c:forEach>
		</select>
	<br/>


	<label for="position_id">役職</label>
		<select name="position_id">
			<option></option>
			<c:forEach items="${positions}" var="positions">
				<option value="${positions.id}" >${positions.position_name}</option>
			</c:forEach>
		</select>
	<br/>

	<input type="submit" value="登録" />
	<br/>


	</form><br/>

	<a href="management">戻る</a>

</body>
</html>
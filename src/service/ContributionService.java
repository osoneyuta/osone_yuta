package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Contribution;
import dao.ContributionDao;

public class ContributionService {

	public void Store (Contribution contribution) {

		Connection connection = null;

		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.insert(connection, contribution);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	public List<Contribution> getContribution(String start, String end, String category) {


		Connection connection = null;

		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			List<Contribution> ret = contributionDao.getContributions(connection, start, end, category);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	public void delete (Contribution contribution) {

		Connection connection = null;

		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.delete(connection, contribution);

			commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}

package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserInfomation;
import service.SetNameService;
import service.SettingService;
import service.UserInfomationService;
import service.UserService;


@WebServlet(urlPatterns = {"/setting"})
public class SettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();


		List<String> messages = new ArrayList<String>();

		if((StringUtils.isBlank(request.getParameter("id")) == true)) {

			List<UserInfomation> infomations = new UserInfomationService().getInfomations();

			request.setAttribute("infomations", infomations);

			messages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", messages);


			response.sendRedirect("management");
			return;
		}

		boolean user = UserService.existence(request.getParameter("id"));

		if (request.getParameter("id").matches("^[0-9]*$") &&
				user == true){

			int id = (int) session.getAttribute("loginUser");

			request.setAttribute("id", request.getParameter("id"));
			request.setAttribute("loginUser", id);


			List<UserInfomation> name = new SetNameService().getUserName(Integer.parseInt(request.getParameter("id")));
			List<UserInfomation> branches = new SetNameService().getBranchNames();
			List<UserInfomation> positions = new SetNameService().getPositionNames();


			request.setAttribute("name", name);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);



			request.getRequestDispatcher("setting.jsp").forward(request, response);

		} else if (StringUtils.isBlank(request.getParameter("id")) == true) {

			List<UserInfomation> infomations = new UserInfomationService().getInfomations();

			request.setAttribute("infomations", infomations);

			messages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", messages);


			response.sendRedirect("management");

		} else {

			List<UserInfomation> infomations = new UserInfomationService().getInfomations();

			request.setAttribute("infomations", infomations);

			messages.add("不正なパラメーターが入力されました");
			session.setAttribute("errorMessages", messages);


			response.sendRedirect("management");
		}






	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		int id = Integer.parseInt(request.getParameter("id"));
		int checkId = (int) session.getAttribute("loginUser");

		if (id == checkId) {

			String login_id = request.getParameter("login_id");
			String username = request.getParameter("name");
			String password = request.getParameter("password");
			String passwordCheck = request.getParameter("password_check");



			User user = new User();
			User userCheck = new User();

			userCheck = UserService.check(login_id);

			if (StringUtils.isBlank(login_id) == true) {

				String notChange = null;
				user.setLoginId(notChange);
			}

			if (StringUtils.isBlank(login_id) != true) {

				if ((login_id.length() >= 6) != true || (login_id.length() <= 20) != true) {

					messages.add("ログインIDは6文字以上20文字以下で入力してください");
				} else if (login_id.matches("^[0-9a-zA-Z]+$") != true) {

					messages.add("ログインIDは半角英数字のみ登録出来ます");

				} else if (userCheck != null) {

					String notChange = null;
					user.setLoginId(notChange);
				} else {

					user.setLoginId(login_id);
				}
			}


			if (StringUtils.isBlank(password) == true && (StringUtils.isBlank(passwordCheck)) == true) {

				String notChange = null;
				user.setPassword(notChange);
			}

			if ((StringUtils.isBlank(password)) != true) {

				if (password.equals(passwordCheck) != true) {

					messages.add("パスワードが合っていません");
				} else if ((password.length() >= 6) != true || (password.length() <= 20) != true) {

					messages.add("パスワードは6文字以上20文字以下で入力してください");
				} else if (password.matches("^[a-zA-Z0-9-/:-@\\[-\\`\\{-\\~]+$") != true) {

					messages.add("パスワードは半角英数字と記号のみ登録出来ます");
				} else {

					user.setPassword(password);
				}
			}

			// idを取得。
			user.setId(id);

			// 名前が空の場合エラーメッセージを表示。
			if (StringUtils.isEmpty(username) != true) {
				user.setName(username);
			} else {
				messages.add("名前を入力してください");
			}

			// passwordの内容を確認して取得。




			// エラーメッセージがあれば、エラーメッセージを表示し編集画面に戻る。
			if (messages.size() == 0 && user.getPassword() != null) {

				new SettingService().selfEdit(user);
				response.sendRedirect("management");
			} else if (messages.size() == 0 && user.getPassword() == null) {

				new SettingService().passwordNotSelfEdit(user);
				response.sendRedirect("management");
			} else {


				List<UserInfomation> name = new SetNameService().getUserName(id);


				request.setAttribute("name", name);
				// request.setAttribute("failName", username);
				request.setAttribute("password", password);
				request.setAttribute("passwordCheck", passwordCheck);


				request.setAttribute("id", id);
				request.setAttribute("messages", messages);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
			}

		}

		if (id != checkId) {

			String login_id = request.getParameter("login_id");
			String username = request.getParameter("name");
			String password = request.getParameter("password");
			String passwordCheck = request.getParameter("password_check");
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			int position_id = Integer.parseInt(request.getParameter("position_id"));


			User user = new User();

			User userCheck = new User();

			userCheck = UserService.check(login_id);

			if (StringUtils.isBlank(login_id) == true) {

				String notChange = null;
				user.setLoginId(notChange);
			}

			if (StringUtils.isBlank(login_id) != true) {

				if ((login_id.length() >= 6) != true || (login_id.length() <= 20) != true) {

					messages.add("ログインIDは6文字以上20文字以下で入力してください");
				} else if (login_id.matches("^[0-9a-zA-Z]+$") != true) {

					messages.add("ログインIDは半角英数字のみ登録出来ます");

				} else if (userCheck != null) {

					String notChange = null;
					user.setLoginId(notChange);
				} else {

					user.setLoginId(login_id);
				}
			}


			if (StringUtils.isBlank(password) == true && (StringUtils.isBlank(passwordCheck)) == true) {

				String notChange = null;
				user.setPassword(notChange);
			}

			if ((StringUtils.isBlank(password)) != true) {

				if (password.equals(passwordCheck) != true) {

					messages.add("パスワードが合っていません");
				} else if ((password.length() >= 6) != true || (password.length() <= 20) != true) {

					messages.add("パスワードは6文字以上20文字以下で入力してください");
				} else if (password.matches("^[a-zA-Z0-9-/:-@\\[-\\`\\{-\\~]+$") != true) {

					messages.add("パスワードは半角英数字と記号のみ登録出来ます");
				} else {

					user.setPassword(password);
				}
			}

			// idを取得。
			user.setId(id);

			// 名前が空の場合エラーメッセージを表示。
			if (StringUtils.isEmpty(username) != true) {
				user.setName(username);
			} else {
				messages.add("名前を入力してください");
			}

			// passwordの内容を確認して取得。

			/*if ((StringUtils.isBlank(password) == true) && (StringUtils.isBlank(passwordCheck) == true)) {

				String notChange = null;
				user.setPassword(notChange);
			} else */

			// branch_idとposition_idの組み合わせをチェック
			if ((branch_id == 1 && (position_id != 1 && position_id != 2)) ||
					(branch_id != 1 && (position_id == 1 || position_id == 2))){

				messages.add("支店名と役職名の組み合わせが不正です");
			} else {
				user.setBranchId(branch_id);
				user.setPositionId(position_id);
			}


			// エラーメッセージがあれば、エラーメッセージを表示し編集画面に戻る。
			if (messages.size() == 0 && user.getPassword() != null) {

				new SettingService().edit(user);
				response.sendRedirect("management");
			} else if (messages.size() == 0 && user.getPassword() == null) {

				new SettingService().passwordNotEdit(user);
				response.sendRedirect("management");
			} else {


				List<UserInfomation> name = new SetNameService().getUserName(id);
				List<UserInfomation> branches = new SetNameService().getBranchNames();
				List<UserInfomation> positions = new SetNameService().getPositionNames();


				request.setAttribute("name", name);
				// request.setAttribute("failName", username);
				request.setAttribute("password", password);
				request.setAttribute("passwordCheck", passwordCheck);
				request.setAttribute("branches", branches);
				request.setAttribute("positions", positions);


				request.setAttribute("id", id);
				request.setAttribute("messages", messages);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
			}


			/*if (StringUtils.isBlank(login_id) == true) {

				String notChange = null;
				user.setLoginId(notChange);
			}

			if (StringUtils.isBlank(login_id) != true) {

				if ((login_id.length() >= 6) != true || (login_id.length() <= 20) != true) {

					messages.add("ログインIDは6文字以上20文字以下で入力してください");
				} else if (login_id.matches("^[0-9a-zA-Z]+$") != true) {

					messages.add("ログインIDは半角英数字のみ登録出来ます");

				} else if (userCheck != null) {

					String notChange = null;
					user.setLoginId(notChange);
				} else {

					user.setLoginId(login_id);
				}
			}


			if (StringUtils.isBlank(password) == true && (StringUtils.isBlank(passwordCheck)) == true) {

				String notChange = null;
				user.setPassword(notChange);
			}

			if ((StringUtils.isBlank(password)) != true) {

				if (password.equals(passwordCheck) != true) {

					messages.add("パスワードが合っていません");
				} else if ((password.length() >= 6) != true || (password.length() <= 20) != true) {

					messages.add("パスワードは6文字以上20文字以下で入力してください");
				} else if (password.matches("^[a-zA-Z0-9-/:-@\\[-\\`\\{-\\~]+$") != true) {

					messages.add("パスワードは半角英数字と記号のみ登録出来ます");
				} else {

					user.setPassword(password);
				}
			}*/
		}
	}

}

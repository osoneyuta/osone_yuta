<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>top</title>

<script type="text/javascript">


function deletecheck(){

	if(window.confirm('削除してよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}

function check(){

	if(window.confirm('コメントしてよろしいですか？')){ // 確認ダイアログを表示

		return true; // 「OK」時は送信を実行

	}
	else{ // 「キャンセル」時の処理

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false; // 送信を中止

	}

}



</script>

<style type="text/css">



.contribution-names{

	border-style: solid;
	background-color: yellow;
	word-wrap: break-word;


	margin: 10 auto;
}

#wrap {

	word-wrap: break-word;
}

.comment-List {

	margin-top: 10px;
	background-color: #B5FF14;
	border-style: solid;
	word-wrap: break-word;

	margin: 10 auto;
}



</style>
</head>
<body>



		<div class="header">
			<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			</c:if>

			<c:if test="${ not empty loginUser }">
			<div class="headline">
				<a href="./">ホーム</a>
				<a href="contribution?id=${loginUser}">新規投稿</a>
				<a href="management">ユーザー管理</a>
				<a href="logout">ログアウト</a>
			</div>

			<div class="search">
			<form action="./" method="get" >
				<input type="date" name="start" value="${start}"/>
				<input type="date" name="end"  value="${end}"/>
				<input type="text" name="category" value="${category }"/>
				<input type="submit" value="絞り込み" />
			</form>
			</div>

			<h3>${loginUserName}さん</h3>
			<input type="hidden" name="is_deleted" value="${is_deleted}" />
			<input type="hidden" name="position_id" value="${position_id}" />
			</c:if>
		</div>

		<c:if test="${ not empty messages }">
		<div class="messages">
			<ul>
			<c:forEach items="${messages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
			</ul>
		</div>
			<c:remove var="messages" scope="session" />
		</c:if>


	<div class="null">
	</div>


	<div class="contributions">

		<c:forEach items="${contributions}" var="contributions">
		<div class="contribution">
			<div class="contribution-names">
				<span class="subject">件名:<c:out value="${contributions.subject}" /></span><br />
				<span class="category">カテゴリー:<c:out value="${contributions.category}" /></span><br />
				<span class="name">名前:<c:out value="${contributions.name}" /></span><br />
				<span>本文:</span><br/>
				<pre><c:out value="${contributions.text }" /></pre>
				<c:out value="${contributions.createdAt}" />
				<c:if test="${contributions.userId == loginUser }">
				<form action="delete" method="post" onSubmit="return deletecheck()">
					<input type="hidden" name="contributions.id" value="${contributions.id}" />
					<input type="submit" value="削除" />
				</form>
				</c:if>
			</div>


		</div>



			<div class="comment">
			<c:forEach items="${comments}" var="comments">
				<c:if test="${contributions.id == comments.contribution_id}">
					<div class="comment-List">
						<div>
							<span class="name">名前:<c:out value="${comments.name}" /></span><br />
							<span>本文:</span>
						</div>
						<pre><c:out value="${comments.text}" /></pre>
						<div class="comment-date">
							<c:out value="${comments.created_at}" />
						</div>

						<c:if test="${comments.user_id == loginUser }">
							<form action="commentdelete" method="post"  onSubmit="return deletecheck()">
								<input type="hidden" name="commentid" value="${comments.id}" />
								<input type="submit" value="コメント削除" />
							</form>
						</c:if>
					</div>
				</c:if>
			</c:forEach>
			</div>


			<div class="post-comment">
			<form action="comment" method="post" onSubmit="return check()" >
				<input type="hidden" name="loginUser" value="${loginUser}" /> <input
					type="hidden" name="contributionId" value="${contributions.id}" />
				<span>コメント記入欄(500文字まで)</span><br />
				<textarea name="text" rows="10" cols="40" >${text}</textarea>
				<br /> <input type="submit" value="コメント">
			</form>
			</div>



		</c:forEach>

	</div>





</body>
</html>
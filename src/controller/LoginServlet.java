package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Contribution;
import beans.User;
import service.CommentService;
import service.ContributionService;
import service.LoginService;

@WebServlet(urlPatterns = {"/login"} )
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;





	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(login_id, password);




		HttpSession session = request.getSession();

		if(user != null && user.getIsDeleted() == 0) {

			session.setAttribute("loginUser", user.getId());
			session.setAttribute("loginUserName", user.getName());
			session.setAttribute("position_id", user.getPositionId());
			session.setAttribute("is_deleted", user.getIsDeleted());

			Date now = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

			String nowDate = sdf.format(now);


			String created_at1 = request.getParameter("created_at1");
			String created_at2 = request.getParameter("created_at2");
			String category = request.getParameter("category");


			if (StringUtils.isBlank(request.getParameter("created_at1")) == true) {
				created_at1 = "2018-01-01-00-00-00";
			} else {
				created_at1 = request.getParameter("created_at1");
			}


			if (StringUtils.isBlank(request.getParameter("created_at2")) == true) {
				created_at2 = nowDate;
			} else {
				created_at2 = request.getParameter("created_at2");
			}




			List<Contribution> contributions = new ContributionService().getContribution(created_at1, created_at2, category);

			request.setAttribute("contributions", contributions);

			List<Comment> comments = new CommentService().getComment();

			request.setAttribute("comments", comments);

			response.sendRedirect("./");



		} else {

			List<String> messages = new ArrayList<String>();
			messages.add("ログインに失敗しました");
			request.setAttribute("login_id", login_id);
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}


	}

}

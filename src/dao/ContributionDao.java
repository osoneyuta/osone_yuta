package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Contribution;
import exception.SQLRuntimeException;

public class ContributionDao {

	public void insert(Connection connection, Contribution contribution) {




		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO contributions (");
			sql.append("subject");
			sql.append(",category");
			sql.append(",text");
			sql.append(",created_at");
			sql.append(",user_id");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(",?");
			sql.append(",?");
			sql.append(",CURRENT_TIMESTAMP");
			sql.append(",?");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, contribution.getSubject());
			ps.setString(2, contribution.getCategory());
			ps.setString(3, contribution.getText());
			ps.setInt(4, contribution.getUserId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	public List<Contribution> getContributions(Connection connection, String start,
			String end, String category) {




		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("contributions.id as id, ");
			sql.append("contributions.subject as subject, ");
			sql.append("contributions.category as category, ");
			sql.append("contributions.text as text, ");
			sql.append("contributions.user_id as user_id, ");
			sql.append("contributions.created_at as created_at, ");
			sql.append("users.name as name ");
			sql.append("FROM contributions ");
			sql.append("INNER JOIN users ");
			sql.append("ON contributions.user_id = users.id ");
			sql.append("WHERE contributions.created_at ");
			sql.append("between ? ");
			sql.append("and ? ");

			if (StringUtils.isBlank(category) != true) {
				sql.append("and contributions.category LIKE ? ");
			}

			sql.append("ORDER BY contributions.id DESC ");


			ps = connection.prepareStatement(sql.toString());



			ps.setString(1, start);
			ps.setString(2, end);

			if (StringUtils.isBlank(category) != true) {
			ps.setString(3, "%" + category + "%");
			}


			ResultSet rs = ps.executeQuery();
			List<Contribution> ret = toContributionList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<Contribution> toContributionList(ResultSet rs) throws SQLException {

		List<Contribution> ret = new ArrayList<Contribution>();

		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String category = rs.getString("category");
				String text = rs.getString("text");
				int user_id = rs.getInt("user_id");
				String name = rs.getString("name");
				Timestamp created_at = rs.getTimestamp("created_at");

				Contribution contribution = new Contribution();
				contribution.setId(id);
				contribution.setSubject(subject);
				contribution.setCategory(category);
				contribution.setText(text);
				contribution.setUserId(user_id);
				contribution.setName(name);
				contribution.setCreatedAt(created_at);

				ret.add(contribution);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void delete (Connection connection, Contribution contribution) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE ");
			sql.append("FROM contributions ");
			sql.append("WHERE ");
			sql.append("id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, contribution.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}

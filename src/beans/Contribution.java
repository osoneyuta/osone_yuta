package beans;

import java.io.Serializable;
import java.sql.Timestamp;


public class Contribution implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String subject;
	private String text;
	private String category;
	private Timestamp created_at;
	private String name;
	private int user_id;

	public void setId(int id) {
		this.id = id;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setCategory (String category) {
		this.category = category;
	}

	public void setCreatedAt (Timestamp created_at) {
		this.created_at = created_at;
	}

	public void setUserId (int user_id) {
		this.user_id = user_id;
	}

	public int getId() {
		return this.id;
	}

	public String getSubject() {
		return this.subject;
	}

	public String getText() {
		return this.text;
	}

	public String getCategory() {
		return this.category;
	}

	public Timestamp getCreatedAt() {
		return this.created_at;
	}

	public int getUserId() {
		return user_id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

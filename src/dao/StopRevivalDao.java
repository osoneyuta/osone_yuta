package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.UserInfomation;
import exception.SQLRuntimeException;

public class StopRevivalDao {

	public void stop (Connection connection, UserInfomation userInfomation) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_deleted = 1 ");
			sql.append("WHERE ");
			sql.append(" id = ? ");

			ps=connection.prepareStatement(sql.toString());

			ps.setInt(1, userInfomation.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	public void revival (Connection connection, UserInfomation userInfomation) {

		PreparedStatement ps = null;

		try {

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_deleted = 0 ");
			sql.append("WHERE ");
			sql.append(" id = ? ");

			ps=connection.prepareStatement(sql.toString());

			ps.setInt(1, userInfomation.getId());
			ps.executeUpdate();

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}
}

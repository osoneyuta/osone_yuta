package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.User;
import exception.SQLRuntimeException;

public class SettingDao {

	public void update(Connection connection,User user) {

		PreparedStatement ps = null;

		if (user.getLoginId() != null && user.getPassword() != null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("login_id = ? ");
				sql.append(",name = ? ");
				sql.append(",password = ? ");
				sql.append(",branch_id = ? ");
				sql.append(",position_id = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		if (user.getLoginId() == null && user.getPassword() != null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("name = ? ");
				sql.append(",password = ? ");
				sql.append(",branch_id = ? ");
				sql.append(",position_id = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getName());
				ps.setString(2, user.getPassword());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		if (user.getLoginId() != null && user.getPassword() == null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("login_id = ? ");
				sql.append(",name = ? ");
				sql.append(",branch_id = ? ");
				sql.append(",position_id = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

		}

		if (user.getLoginId() == null && user.getPassword() == null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("name = ? ");
				sql.append(",branch_id = ? ");
				sql.append(",position_id = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getName());
				ps.setInt(2, user.getBranchId());
				ps.setInt(3, user.getPositionId());
				ps.setInt(4, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

		}
	}

	public void selfUpdate(Connection connection,User user) {

		PreparedStatement ps = null;

		if (user.getLoginId() != null && user.getPassword() != null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("login_id = ? ");
				sql.append(",name = ? ");
				sql.append(",password = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		if (user.getLoginId() == null && user.getPassword() != null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("name = ? ");
				sql.append(",password = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getName());
				ps.setString(2, user.getPassword());
				ps.setInt(3, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}

		if (user.getLoginId() != null && user.getPassword() == null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("login_id = ? ");
				sql.append(",name = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getLoginId());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

		}

		if (user.getLoginId() == null && user.getPassword() == null) {

			try {
				StringBuilder sql = new StringBuilder();

				sql.append("UPDATE users SET ");
				sql.append("name = ? ");
				sql.append("WHERE");
				sql.append(" id = ? ");


				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getName());
				ps.setInt(2, user.getId());
				ps.executeUpdate();



			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
	}
}

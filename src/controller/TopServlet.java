package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.Contribution;
import service.CommentService;
import service.ContributionService;



@WebServlet(urlPatterns= {"/index.jsp"})
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {



		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

		String nowDate = sdf.format(now);


		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String category = request.getParameter("category");


		if (StringUtils.isBlank(request.getParameter("start")) == true) {
			start = "2018-01-01-00-00-00";
		} else {
			start = request.getParameter("start") + "-00-00-00";
		}


		if (StringUtils.isBlank(request.getParameter("end")) == true) {
			end = nowDate;
		} else {
			end = request.getParameter("end") + "-23-59-59";
		}




		List<Contribution> contributions = new ContributionService().getContribution(start, end, category);

		request.setAttribute("contributions", contributions);

		List<Comment> comments = new CommentService().getComment();

		request.setAttribute("comments", comments);

		if (StringUtils.isBlank(start) != true) {

			request.setAttribute("start", request.getParameter("start"));
		}

		if (StringUtils.isBlank(end) != true) {

			request.setAttribute("end", request.getParameter("end"));
		}

		if (StringUtils.isBlank("category") != true) {

			request.setAttribute("category", request.getParameter("category"));
		}

		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.sendRedirect("top.jsp");
	}

}

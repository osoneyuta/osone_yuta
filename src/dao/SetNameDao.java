/**
 *
 */
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserInfomation;
import exception.SQLRuntimeException;

public class SetNameDao {

	public List<UserInfomation> getPositionNames(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("positions.id, ");
			sql.append("positions.name ");
			sql.append("FROM ");
			sql.append("positions ");




			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserInfomation> ret = toPositionNames(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}


	private List<UserInfomation> toPositionNames(ResultSet rs)
			throws SQLException {

		List<UserInfomation> ret = new ArrayList<UserInfomation>();

		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");

				UserInfomation infomation = new UserInfomation();
				infomation.setId(id);
				infomation.setPosition_name(name);

				ret.add(infomation);
			}


			return ret;

		} finally {
			close(rs);
		}
	}


	public List<UserInfomation> getBranchNames(Connection connection) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("branches.id, ");
			sql.append("branches.name ");
			sql.append("FROM ");
			sql.append("branches ");




			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserInfomation> ret = toBranchesNames(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}


	private List<UserInfomation> toBranchesNames(ResultSet rs)
			throws SQLException {

		List<UserInfomation> ret = new ArrayList<UserInfomation>();

		try {
			while (rs.next()) {

				int id = rs.getInt("id");
				String name = rs.getString("name");

				UserInfomation infomation = new UserInfomation();
				infomation.setId(id);
				infomation.setBranch_name(name);

				ret.add(infomation);
			}


			return ret;

		} finally {
			close(rs);
		}
	}

	public List<UserInfomation> getUserName(Connection connection, int id) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.login_id, ");
			sql.append("users.name, ");
			sql.append("users.branch_id, ");
			sql.append("users.position_id ");
			sql.append("FROM ");
			sql.append("users ");
			sql.append("WHERE ");
			sql.append("id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);


			ResultSet rs = ps.executeQuery();
			List<UserInfomation> ret = toUserName(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<UserInfomation> toUserName(ResultSet rs)
			throws SQLException {

		List<UserInfomation> ret = new ArrayList<UserInfomation>();



		try {
			while (rs.next()) {

				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");

				UserInfomation userInfomation = new UserInfomation();
				userInfomation.setLogin_id(login_id);
				userInfomation.setName(name);
				userInfomation.setBranch_id(branch_id);
				userInfomation.setPosition_id(position_id);


				ret.add(userInfomation);
			}


			return ret;

		} finally {
			close(rs);
		}
	}
}

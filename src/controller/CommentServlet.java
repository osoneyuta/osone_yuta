package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import service.CommentService;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet(urlPatterns = {"/comment"})
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		String text = request.getParameter("text");

		System.out.println(text);

		// 改行コードを取り除いた文字列を配列に格納。
		String[] noNewLineText = request.getParameter("text").split("\r\n");

		// textの文字数を確認したい為、0で初期化。
		int textCount = 0;

		// textCountに文字数を足していく。
		for (int i = 0; i < noNewLineText.length; i++) {
			textCount += noNewLineText[i].length();
		}


		if (StringUtils.isBlank(text) == true) {

			messages.add("コメントが空です");
		}

		if (StringUtils.isBlank(text) != true) {

			if (textCount > 500) {

				messages.add("コメントは500文字以内で入力してください");
			}
		}



		if (messages.size() == 0) {
			Comment comment = new Comment();
			comment.setContribution_id(Integer.parseInt(request.getParameter("contributionId")));
			comment.setText(request.getParameter("text"));
			comment.setUser_id(Integer.parseInt(request.getParameter("loginUser")));

			new CommentService().Store(comment);

			response.sendRedirect("./");
		} else {

			request.setAttribute("text", request.getParameter("text"));
			session.setAttribute("messages", messages);

			response.sendRedirect("./");
		}
	}

}

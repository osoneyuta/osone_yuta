package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserInfomation;
import service.SetNameService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<UserInfomation> branches = new SetNameService().getBranchNames();
		List<UserInfomation> positions = new SetNameService().getPositionNames();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();



		if (isValid(request,messages) == true) {
			User user = new User();
			user.setLoginId(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
			user.setPositionId(Integer.parseInt(request.getParameter("position_id")));

			new UserService().register(user);

			response.sendRedirect("management");

		} else {

			List<UserInfomation> branches = new SetNameService().getBranchNames();
			List<UserInfomation> positions = new SetNameService().getPositionNames();

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);

			request.setAttribute("errorMessages", messages);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("password_check");
		String name = request.getParameter("name");
		int branch_id = 0;
		int position_id = 0;

		String branchCheck = request.getParameter("branch_id");
		String positionCheck = request.getParameter("position_id");

		User user = UserService.check(login_id);

		if (user != null) {
			messages.add("このログインIDは使用されています");
		}


		if (StringUtils.isBlank(login_id) == true) {

			messages.add("ログインIDを入力してください");
		}


		if (StringUtils.isBlank(login_id) != true) {
			if ((login_id.length() >= 6) != true || (login_id.length() <= 20) != true) {

				messages.add("ログインIDは6文字以上20文字以下で入力してください");
			} else if (login_id.matches("^[0-9a-zA-Z]+$") != true) {

				messages.add("ログインIDは半角英数字のみ登録出来ます");
			}
		}


		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (password.length() >= 6 && password.length() <= 20 != true) {

			messages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if (password.matches("^[¥x20-¥x7F]+$") != true ||
				passwordCheck.matches("^[¥x20-¥x7F]+$") != true) {

			messages.add("パスワードは半角英数字と記号のみ登録出来ます");
		}

		if (password.equals(passwordCheck) != true) {
			messages.add("パスワードが合っていません");
		}

		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}

		/*if (login_id.length() >= 6 && login_id.length() <= 20 != true) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}*/

		/*if (login_id.matches("^[0-9a-zA-Z]+$") != true) {

			messages.add("ログインIDは半角英数字のみ登録出来ます");
		}*/



		/*if (password.length() >= 6 && password.length() <= 20 != true) {

			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}*/

		/*if (password.matches("^[¥x20-¥x7F]+$") ||
				passwordCheck.matches("^[¥x20-¥x7F]+$") != true) {

			messages.add("パスワードは半角英数字と記号のみ登録出来ます");
		}*/



		if (StringUtils.isBlank(branchCheck) == true &&
				StringUtils.isBlank(positionCheck) == true) {

			messages.add("支店と役職を選択してください");

		} else if (StringUtils.isBlank(branchCheck) == true &&
				StringUtils.isBlank(positionCheck) != true) {

			messages.add("役職を選択してください");
		} else if (StringUtils.isBlank(branchCheck) != true &&
				StringUtils.isBlank(positionCheck) == true) {

			messages.add("支店を選択してください");
		} else {

			branch_id = Integer.parseInt(request.getParameter("branch_id"));
			position_id = Integer.parseInt(request.getParameter("position_id"));
		}

		if ((branch_id == 1 && (position_id != 1 && position_id != 2)) ||
				(branch_id != 1 && (position_id == 1 || position_id == 2))){

			messages.add("支店名と役職名の組み合わせが不正です");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}

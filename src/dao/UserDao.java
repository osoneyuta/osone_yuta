package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
            sql.append("login_id ");
            sql.append(", password ");
            sql.append(", name ");
            sql.append(", branch_id ");
            sql.append(", position_id ");
            sql.append(") VALUES ( ");
            sql.append("? "); // login_id
            sql.append(", ? "); // password
            sql.append(", ? "); // name
            sql.append(", ? "); // branch_id
            sql.append(", ? "); // position_id
            sql.append(") ");

            ps = connection.prepareStatement(sql.toString());


            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.executeUpdate();

		} catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}


	public User getUser(Connection connection, String login_id, String password) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true) {

				return null;
			} else if(2 <= userList.size()) {

				throw new IllegalStateException("2 <= userList.size()");
			} else {

				return userList.get(0);
			}

		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		} finally {

			close(ps);
		}

	}


	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {

				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branch_id = rs.getInt("branch_id");
				int position_id = rs.getInt("position_id");
				int is_deleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginId(login_id);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branch_id);
				user.setPositionId(position_id);
				user.setIsDeleted(is_deleted);

				ret.add(user);
			}

			return ret;

		} finally {
			close(rs);
		}


	 }

	public User checkId(Connection connection, String login_id) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT users.login_id FROM users WHERE login_id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);


			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserLoginId(rs);

			if(userList.isEmpty() == true) {

				return null;
			} else {

				return userList.get(0);
			}

		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		} finally {

			close(ps);
		}
	}

	private List<User> toUserLoginId(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {

				String login_id = rs.getString("login_id");

				User user = new User();

				user.setLoginId(login_id);

				ret.add(user);
			}

			return ret;

		} finally {
			close(rs);
		}


	 }









	public boolean existenceId(Connection connection, String id) {

		PreparedStatement ps = null;

		try {
			String sql = "SELECT users.id FROM users WHERE id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserId(rs);

			if(userList.size() == 1) {

				return true;
			} else {

				return false;
			}

		} catch (SQLException e) {

			throw new SQLRuntimeException(e);
		} finally {

			close(ps);
		}
	}

	private List<User> toUserId(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();

		try {
			while(rs.next()) {

				int id = rs.getInt("id");

				User user = new User();
				user.setId(id);

				ret.add(user);
			}

			return ret;

		} finally {
			close(rs);
		}


	 }



}

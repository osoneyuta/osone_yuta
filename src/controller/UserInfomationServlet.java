package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserInfomation;
import service.UserInfomationService;

@WebServlet(urlPatterns = {"/management"})
public class UserInfomationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;




	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		List<UserInfomation> infomations = new UserInfomationService().getInfomations();

		request.setAttribute("infomations", infomations);


		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}
}
